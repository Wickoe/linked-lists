package opgave1;

public class SortedLinkedList {
	private Node first;

	private class Node implements Comparable<Node> {
		private Node next;
		private String data;

		public Node next() {
			return next;
		}

		public void setNext(Node next) {
			this.next = next;
		}

		public String getData() {
			return data;
		}

		public void setData(String data) {
			this.data = data;
		}

		@Override
		public int compareTo(Node o) {
			return data.compareToIgnoreCase(o.getData());
		}
	}

	public SortedLinkedList() {
		first = null;
	}

	/**
	 * Tilf�jer et element til listen, s� listen fortsat er sorteret i henhold
	 * til den naturlige ordning p� elementerne.
	 * 
	 * @param element
	 *            det der inds�ttes
	 */
	public void addElement(String element) {
		Node newNode = new Node();
		newNode.setData(element);

		Node at = first;
		Node previous = null;
		boolean inserted = false;

		while (!inserted && at != null) {
			if (newNode.compareTo(at) <= 0) {
				inserted = true;
			} else {
				previous = at;
				at = at.next();
			}
		}

		if (at == first) {
			first = newNode;
		} else {
			previous.setNext(newNode);
		}
		newNode.setNext(at);
	}

	/**
	 * Fjerner et element fra listen.
	 * 
	 * @param element
	 *            det element der fjernes
	 * @return true hvis elementet blev fjernet, men ellers false.
	 */
	public boolean removeElement(String element) {
		boolean removed = false;
		Node at = first;
		Node previous = null;

		while (!removed && at != null) {
			if (!at.getData().equalsIgnoreCase(element)) {
				previous = at;
				at = at.next();
			} else {
				if (previous != null) {
					previous.setNext(at.next());
				} else {
					first = at.next();
				}
				removed = true;
			}
		}

		return removed;
	}

	/**
	 * Beregner antallet af elementer i listen.
	 */
	public int countElements() {
		Node at = first;
		int count = 0;

		while (at != null) {
			count++;
			at = at.next();
		}

		return count;
	}

	@Override
	public String toString() {
		String toString = first.getData();
		Node at = first.next();

		while (at != null) {
			toString += ", " + at.getData();
			at = at.next();
		}

		return toString;
	}
}
