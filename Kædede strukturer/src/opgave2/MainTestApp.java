package opgave2;

public class MainTestApp {
	public static void main(String[] args) {
		String michael = "Michael";
		String martin = "Martin";
		String nicolai = "Nicolai";
		String leo = "Leo";
		String chriss = "Chriss";
		String max = "Max";

		SortedLinkedList sll = new SortedLinkedList();

		sll.addElement(michael);
		System.out.println(sll);
		System.out.println();

		sll.addElement(martin);
		System.out.println(sll);
		System.out.println();

		sll.addElement(nicolai);
		System.out.println(sll);
		System.out.println();

		sll.addElement(leo);
		System.out.println(sll);
		System.out.println();

		sll.addElement(chriss);
		System.out.println(sll);
		System.out.println();

		System.out.print("Removing 'Martin': ");
		System.out.println(sll.removeElement("Martin") + "\n");

		sll.addElement(max);
		System.out.println(sll);
		System.out.println();
	}
}
