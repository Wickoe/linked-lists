package opgave2;

public class SortedLinkedList {
	private DoubleNode first;
	private DoubleNode last;
	
	private class DoubleNode implements Comparable<DoubleNode> {
		private DoubleNode next;
		private DoubleNode previous;
		
		private String data;
		
		@Override
		public int compareTo(DoubleNode o) {
			return data.compareToIgnoreCase(o.data);
		}
	}
	
	public SortedLinkedList() {
		first = new DoubleNode();
		last = new DoubleNode();
		
		first.next = last;
		last.previous = first;
	}
	
	/**
	 * Tilf�jer et element til listen, s� listen fortsat er sorteret i henhold
	 * til den naturlige ordning p� elementerne.
	 *
	 * @param element
	 *            det der inds�ttes
	 */
	public void addElement(String element) {
		DoubleNode newNode = new DoubleNode();
		newNode.data = element;
		
		DoubleNode at = first.next;
		boolean inserted = false;
		
		while (!inserted && at != last) {
			if (newNode.compareTo(at) <= 0) {
				inserted = true;
			}
			else {
				at = at.next;
			}
		}
		
		at = at.previous;
		
		newNode.next = at.next;
		at.next = newNode;
		newNode.previous = at;
		newNode.next.previous = newNode;
	}
	
	/**
	 * Fjerner et element fra listen.
	 *
	 * @param element
	 *            det element der fjernes
	 * @return true hvis elementet blev fjernet, men ellers false.
	 */
	public boolean removeElement(String element) {
		DoubleNode at = first.next;
		boolean removed = false;
		
		while (!removed && at != last) {
			if (!element.equalsIgnoreCase(at.data)) {
				at = at.next;
			}
			else {
				at.next.previous = at.previous;
				at.previous.next = at.next;
				removed = true;
			}
		}
		
		return removed;
	}
	
	/**
	 * Beregner antallet af elementer i listen.
	 */
	public int countElements() {
		DoubleNode at = first;
		int count = 0;
		
		while (at != null) {
			count++;
			at = at.next;
		}
		
		return count;
	}
	
	@Override
	public String toString() {
		DoubleNode at = first.next;
		String toString = at.data;
		
		at = at.next;
		
		while (at != last) {
			toString += ", " + at.data;
			at = at.next;
		}
		
		return toString;
	}
}