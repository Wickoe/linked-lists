package opgave3;

public class MainTestApp {
	public static void main(String[] args) {
		String michael = "Michael";
		String nicolai = "Nicolai";
		String leo = "Leo";
		String chriss = "Chriss";
		String max = "Max";

		SortedLinkedList sll1 = new SortedLinkedList();

		sll1.addElement(michael);
		sll1.addElement(nicolai);
		sll1.addElement(leo);
		sll1.addElement(chriss);
		sll1.addElement(max);

		SortedLinkedList sll2 = new SortedLinkedList();
		String martin = "Martin";
		String aage = "�ge";

		sll2.addElement(martin);
		sll2.addElement(aage);

		System.out.println(sll1);
		sll1.addAll(sll2);
		System.out.println(sll1);
	}
}
